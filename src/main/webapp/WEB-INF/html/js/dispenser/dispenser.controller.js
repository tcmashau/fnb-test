(function() {
  'use strict';

  angular
    .module('app.dispenser')
    .controller('DispenserController', DispenserController);

  DispenserController.$inject = ['DispenserDataService'];
  /* @ngInject */
  function DispenserController(DispenserDataService) {
    var vm = this;
    vm.breakdown = breakdown;

    function breakdown() {

        DispenserDataService.breakdown(vm.amount).then(function (response) {

          if (response.status != 200) {
              vm.errors = response.data.errors;
              if (response.data.message != undefined && vm.errors == undefined) {
                  vm.errors = [{
                      field: "",
                      defaultMessage: response.data.message
                  }]
              }
          }else {
              vm.result = response.data;

          }

      });

    }

  }
})();

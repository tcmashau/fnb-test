(function() {
  'use strict';

  angular
    .module('app.dispenser')
    .factory('DispenserDataService', DispenserDataService);

  DispenserDataService.$inject = ['$http'];
  /* @ngInject */
  function DispenserDataService($http) {

    var service = {
      breakdown: breakdown
    };

    return service;


    function breakdown(amount) {
      return $http.get('/fnb/dispenser/breakdown',{
          params: {
              amount: amount
          }
      }).then(success)
          .catch(fail);

    }

      function success(response) {
          return response;
      }

      function fail(e) {
          return e;
      }
  }
})();

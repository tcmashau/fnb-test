(function () {
    'use strict';
    angular.module('app', [
        'ui.router',
        'app.user',
        'app.dispenser'])
        .config(['$stateProvider', '$urlRouterProvider','$httpProvider',
        function($stateProvider, $urlRouterProvider,$httpProvider) {
            $httpProvider.interceptors.push('authInterceptor');
            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: 'js/user/login.html',
                    controller:'UserController',
                    controllerAs:'vm'
                }).state('dispense', {
                url: '/dispense',
                templateUrl: 'js/dispenser/dispenser.html',
                controller:'DispenserController',
                controllerAs:'vm'
                });
            $urlRouterProvider.otherwise('/');
        }]);
})();

(function () {
  'use strict';

  angular
    .module('app')
    .factory('authInterceptor', authInterceptor);

  authInterceptor.$inject = ['$q', '$window', '$rootScope'];
  /* @ngInject */
  function authInterceptor($q, $window, $rootScope) {

    return {
      request: function (config) {
        config.headers = config.headers || {};
        if ($window.sessionStorage.token != undefined) {
          config.headers.X_TOKEN = $window.sessionStorage.token;
        }
        return config;
      },
      response: function (response) {
        return response || $q.when(response);
      },
      responseError: function (errorResponse) {

        if (errorResponse.status === 401) {
          $rootScope.loggedIn=false;
        }
        return errorResponse || $q.when(errorResponse);
      }
    };

  }

})();

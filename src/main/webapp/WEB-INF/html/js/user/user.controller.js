(function () {
    'use strict';

    angular
        .module('app.user')
        .controller('UserController', UserController);

    UserController.$inject = ['UserDataService','$state'];
    /* @ngInject */
    function UserController(UserDataService,$state) {
        var vm = this;
        vm.login = login;

        function login() {

            UserDataService.login(vm.username, vm.password).then(function (response) {
                if (response.status != 200) {
                    vm.errors = response.data.errors;
                    if (response.data.message != undefined && vm.errors == undefined) {
                        vm.errors = [{
                            field: "",
                            defaultMessage: response.data.message
                        }]
                    }
                }else {
                    console.log("hoing");
                    $state.go("dispense");
                }
            });

        }

    }
})();

(function () {
    'use strict';

    angular
        .module('app.user')
        .factory('UserDataService', UserDataService);

    UserDataService.$inject = ['$http', '$window'];
    /* @ngInject */
    function UserDataService($http, $window) {

        var service = {
            login: login
        };

        return service;


        function login(username, password) {

            var user = {
                username: username,
                password: password
            };

            return $http.post('/fnb/users/login', user)
                .then(function (response) {
                    if (response.status == 200) {
                        $window.sessionStorage.token = response.data.token;
                    }

                    return response;
                })
                .catch(fail);

        }

        function success(response) {
            return response;
        }

        function fail(e) {
            return e;
        }
    }
})();

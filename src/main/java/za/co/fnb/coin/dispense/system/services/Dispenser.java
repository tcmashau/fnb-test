package za.co.fnb.coin.dispense.system.services;

import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;
/**
 * Created by caesarmashau on 2017/05/16.
 */
@Component
public class Dispenser {

    public enum Denomination {
        HundredRand(100),FiftyRand(50),TwentyRand(20),TenRand(10),FiveRand(5),TwoRand(2),OneRand(1),FiftyCent(0.5),TwentyFiveCent(0.25),
        TenCent(0.1),FiveCent(0.05);

        private final double amount;

        private Denomination(double amount) {
            this.amount = amount;
        }

        public double getAmount() {
            return amount;
        }
    }


    private double getBreakDown(double amount,Map<Denomination,Double> map) {

        double val = amount;
        for(Denomination denomination : Denomination.values()) {

            if(val >= denomination.getAmount()) {

                double reminder = val %denomination.getAmount();
                if(reminder > 0) {
                    val = val-reminder;
                    val = val/denomination.getAmount();
                    map.put(denomination,val);
                    val = reminder;
                }else {
                    val = val/denomination.getAmount();
                    map.put(denomination,val);
                    val =0;
                }
                return  val;

            }

        }
        return val;
    }

    public Map<Denomination,Double> getBreakDown(double amount) {
        Map<Denomination,Double> map = new LinkedHashMap<Denomination, Double>();
        Dispenser dispenser = new Dispenser();
        double val = amount;
        while(val > 0) {
            val = dispenser.getBreakDown(val,map);
        }
        return map;

    }

}

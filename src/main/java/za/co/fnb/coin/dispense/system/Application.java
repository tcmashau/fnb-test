package za.co.fnb.coin.dispense.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
/**
 * Created by caesarmashau on 2017/05/16.
 */
@ComponentScan(value = "za.co.fnb.coin.dispense.system")
@SpringBootApplication
public class Application   extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}


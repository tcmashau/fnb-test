package za.co.fnb.coin.dispense.system.services;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by caesarmashau on 2017/01/03.
 */
@Component
public class Users {

    @Value("${system.username}")
    private String username;

    @Value("${system.password}")
    private String password;

    public Set<String> sessions = new LinkedHashSet<String>();

    public String authenticate(String username,String password) throws Exception {

        if(StringUtils.equalsIgnoreCase(this.username,username) && StringUtils.equalsIgnoreCase(this.password,password)) {

            String key = RandomStringUtils.random(10,true,false);
            sessions.add(key);
            return key;
        }
        throw new Exception("invalid login details");
    }


    public boolean isValid(String key) {
        return sessions.contains(key);
    }
}

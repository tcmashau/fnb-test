package za.co.fnb.coin.dispense.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import za.co.fnb.coin.dispense.system.controller.presentation.UserBean;
import org.springframework.web.bind.annotation.*;
import za.co.fnb.coin.dispense.system.services.Users;
import javax.validation.Valid;

@RequestMapping("/users")
@RestController
public class UsersController  {

    @Autowired
    private Users users;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public UserBean login(@Valid @RequestBody UserBean userBean) throws Exception {

        UserBean ret = new UserBean();
        ret.setToken(users.authenticate(userBean.getUsername(),userBean.getPassword()));
        return ret;

    }

}

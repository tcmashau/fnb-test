package za.co.fnb.coin.dispense.system.controller.presentation;

import org.hibernate.validator.constraints.NotBlank;

public class UserBean {

    @NotBlank
    private String username;

    @NotBlank
    private String password;


    private String token;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

package za.co.fnb.coin.dispense.system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by caesarmashau on 2017/05/18.
 */
@Controller
public class BaseController {

    @RequestMapping("/")
    String home() {
        return "index";
    }

}

package za.co.fnb.coin.dispense.system.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import za.co.fnb.coin.dispense.system.services.Dispenser;
import za.co.fnb.coin.dispense.system.services.Users;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by caesarmashau on 2017/05/17.
 */
@RequestMapping("/dispenser")
@RestController
public class DispenserController {

    @Autowired
    private Users users;

    @Autowired
    private Dispenser dispenser;

    @RequestMapping(value = "/breakdown", method = RequestMethod.GET)
    public Map<Dispenser.Denomination,Double> getBreakDown(@RequestParam(required = false, value = "amount") double amount,HttpServletRequest httpServletRequest) throws AuthenticationException {

        String token = httpServletRequest.getHeader("X_TOKEN");
        if(StringUtils.isEmpty(token)) {
            throw new AuthenticationException("invalid session");
        }
        if(!users.isValid(token)) {
            throw new AuthenticationException("invalid session");
        }
        return dispenser.getBreakDown(amount);

    }
}

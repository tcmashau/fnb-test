package za.co.fnb.coin.dispense.system.services;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by caesarmashau on 2017/05/19.
 */
public class UsersTest {

    Users users  = new Users();

    @Test
    public void authenticate() throws Exception {

        String result = users.authenticate("tcmashau","password");
        Assert.assertNotNull(result);
    }

    @Test
    public void isValid() throws Exception {
        boolean result = users.isValid("sadsda");
        Assert.assertFalse(result);
    }

}

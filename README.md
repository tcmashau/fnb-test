## Technology stack used for the solution

* Maven
* Spring boot
* Angular 1
* JUNIT 4
* Swagger


## How to get the project running

* The project uses port 8080. Before running the application please make sure no other application is running on this port.

* Build the project using mvn clean package.

* Execute java -jar fnb-msrv.jar in the final target folder where the project was build.


## Services Documentation

* The application's services are self documenting using swagger, which can be accessed through http://localhost:8080/fnb/swagger-ui.html once the application has started.

## Application UI

* The application's ui is built on angular 1. The ui can be accessed through http://localhost:8080/fnb/


## Login details

Since this is for demo purposes the username and password are placed in the properties file under the resources folder.
Using database would have been out of scope.

username : tcmashau
password : password1


## Authors

* Thiofhi Caesar Mashau
* tcmashau@gmail.com
* 0723994851

